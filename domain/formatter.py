from enum import Enum
from .report import Report
import abc
import json


# Using Enum here is more or less generic way to control the types in application
class OutputFormat(Enum):
    Json = 0,
    Csv = 1


class Formatter(abc.ABC):
    @abc.abstractstaticmethod
    def report(report: Report):
        pass


class JsonFormatter(Formatter):
    @staticmethod
    def report(report):
        data = {
            'top_keywords': report.top_keywords(0.2),
            'bottom_keywords': report.bottom_keywords(0.2)
        }

        return json.dumps(data)

class CsvFormatter(Formatter):
    # To be implemented in the future
    pass