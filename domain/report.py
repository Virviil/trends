import abc
# from .formatter import Formatter
from .page import Page
from collections import defaultdict
import itertools


class Report(abc.ABC):
    @abc.abstractmethod
    def collect(self, page: Page):
        pass

    def generate_report(self, filename: str, formatter):
        report = formatter.report(self)

        with open(filename, 'w') as f:
            f.write(report)

    @abc.abstractmethod
    def top_keywords(self, persentage):
        pass

    @abc.abstractmethod
    def bottom_keywords(self, persentage):
        pass

    @abc.abstractmethod
    def top_domain(self, persentage):
        pass

    @abc.abstractmethod
    def bottom_domains(self, persentage):
        pass


class InMemoryReport(Report):
    def __init__(self):
        self.keywords = defaultdict(int)
        self.domains = defaultdict(int)

    def collect(self, page):
        # Keywords
        for word in page.keywords:
            self.keywords[word] += 1

        # Domains
        self.domains[page.domain] += 1

    def top_keywords(self, persentage):
        itertools.islice(sorted(self.keywords.items(), key=lambda v: v,
                                reverse=True), int(len(self.keywords)*persentage))

    def bottom_keywords(self, persentage):
        itertools.islice(sorted(self.keywords.items(), key=lambda v: v,
                                reverse=False), int(len(self.keywords)*persentage))

    def top_domain(self, persentage):
        itertools.islice(sorted(self.domains.items(), key=lambda v: v,
                                reverse=True), int(len(self.keywords)*persentage))

    def bottom_domains(self, persentage):
        itertools.islice(sorted(self.domains.items(), key=lambda v: v,
                                reverse=False), int(len(self.keywords)*persentage))
