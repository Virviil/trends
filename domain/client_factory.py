from aylienapiclient import textapi
import os


# Here we'll crash the application if the ENV not provided, but this process can be handled more in the future - if needed
applicationId = os.environ['APPLICATION_ID']
applicationKey = os.environ['APPLICATION_KEY']

# This abstraction - with factory - is made specificly to controll the process of client creation.
# May be in future the pool of clients will be used - instead of separate client for each url.
# In this case, Factory can controll the pool and allocate clients and parallel sockets in order to
# make concurrent operations stable

class ClientFactory():
    @staticmethod
    def make():
        return textapi.Client(applicationId, applicationKey)