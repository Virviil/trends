from urllib.parse import urlparse
import validators
from .client_factory import ClientFactory

from enum import Enum


# We are using enum here in order to controll types of the page
# Invalid and Empty are generally the same in current architecture and tasks, but may start to differ
# in next milestone of application improvement.
# Also it can be used more general in Logging
class PageStatus(Enum):
    Valid = 0,
    Invalid = 1,
    Empty = 2


class Page:
    # We can make all preparations that are not touching longterm NET operations in constructor
    def __init__(self, url: str):
        self.raw_url = url.strip()

        if validators.url(self.raw_url):
            self.status = PageStatus.Valid
            url = urlparse(self.raw_url)
            self.domain = url.netloc
        else:
            self.status = PageStatus.Invalid

    # We are performing network operation in separate function in order to encapsulate all longterm processes in one place
    def scrape(self):
        if self.status != PageStatus.Invalid:
            entities = ClientFactory.make().Entities({'url': self.raw_url})
            self.keywords = entities.get('entities', {}).get('keyword', [])
            if self.keywords == []:
                self.status = PageStatus.Empty