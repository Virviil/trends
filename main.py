#!/usr/bin/env python

import os
from urllib.parse import urlparse

from domain.formatter import OutputFormat, JsonFormatter, CsvFormatter
from domain.page import Page
from domain.report import InMemoryReport


output_format = OutputFormat[os.environ.get('OUTPUT_FORMAT', 'Json')]

formatter = {
    OutputFormat.Json: JsonFormatter,
    OutputFormat.Csv: CsvFormatter
}[output_format]


input_file = os.environ['INPUT_FILE']
output_file = os.environ['OUTPUT_FILE']

with open(input_file, 'rU') as f:
    report = InMemoryReport()
    for line in f:
        page = Page(line)
        page.scrape()
        report.collect(page)

    report.generate_report(output_file, formatter)
